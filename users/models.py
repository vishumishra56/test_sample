from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.conf import settings
from django.utils import timezone
from datetime import datetime
from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver
from django.urls import reverse
from django_rest_passwordreset.signals import reset_password_token_created
from django.contrib.auth.models import Permission
from django.contrib.auth.models import Group
from django.core.mail import send_mail  


class UserManager(BaseUserManager):
    def _create_user(self, email, password, **kwargs):
        pl = Permission.objects.filter(codename__in=["view_student"])
        if not email:
            raise ValueError('Email address is required')
        email = self.normalize_email(email)
        user = self.model(email=email, **kwargs)
        user.set_password(password)
        if user.is_superuser:
            print('here')
            student_group, created = Group.objects.get_or_create(name='Student')
            student_group.permissions.set(pl)

        user.save()
        return user

    def create_user(self, email, password, **kwargs):
        kwargs.setdefault('is_staff', False)
        kwargs.setdefault('is_superuser', False)
        kwargs.setdefault('is_active', True)
        return self._create_user(email, password, **kwargs)

    def create_superuser(self, email, password, **kwargs):
        kwargs.setdefault('is_staff', True)
        kwargs.setdefault('is_superuser', True)
        kwargs.setdefault('is_active', True)

        if kwargs.get('is_staff') is not True:
            raise ValueError('Superuser must be a staffself.')
        if kwargs.get('is_superuser') is not True:
            raise ValueError('Superuser must be a superuser')
        return self._create_user(email, password, **kwargs)


class User(AbstractBaseUser, PermissionsMixin):
    USER_TYPE = (
        ('S', 'Student'),
        ('T', 'Teacher'),
        ('A', 'Admin'),
    )

    username = None
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    user_type = models.CharField(max_length=1, choices=USER_TYPE, blank = True, null =True)
    email = models.EmailField(_('email address'), unique=True)
    mobile = models.CharField(max_length=10, blank = True, null =True)
    first_name = models.CharField(max_length=45, blank=True, null=True)
    last_name = models.CharField(max_length=45, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    USERNAME_FIELD = 'email'
    objects = UserManager()


class Student(models.Model):
    user = models.OneToOneField('users.User',on_delete=models.CASCADE)
    def __str__(self):
        return self.user.first_name

class Teacher(models.Model):
    user = models.OneToOneField('users.User',on_delete=models.CASCADE)
    def __str__(self):
        return self.user.first_name

@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):

    # email_plaintext_message = "{}?token={}".format(reverse('password_reset:reset-password-request'), reset_password_token.key)
    text_message = """
    You're receiving this mail because you requested a
    password reset on your user account at 
    127.0.0.1::8000

    Please go to the reset password page and submit 
    your new password and the token mentioned 
    below.

    Your Token is.
    {}

    Your email, in case you've forgotten:
    {}

    Thanks for visiting our App!

    Test Team 
    """.format(reset_password_token.key,reset_password_token.user.email)
    send_mail(
        # title:
        "Password Reset for {title}".format(title="Some website title"),
        # message:
        text_message,
        # from:
        settings.EMAIL_HOST_USER,
        # to:
        [reset_password_token.user.email]
    )
