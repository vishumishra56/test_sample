from django.shortcuts import render

# Create your views here.
from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model 
from rest_framework.views import APIView
from .serializers import UserSerializer,StudentSerializer
from .models import Student
from rest_framework.response import Response
class CreateUserView(CreateAPIView):

    model = get_user_model()
    permission_classes = [
        permissions.AllowAny 
    ]
    serializer_class = UserSerializer



class StudentView(APIView):
    def get(self, request, *args, **kwargs):
        try:
            if request.user.user_type == 'S' or request.user.user_type == 'T':
                print(request.user.user_type)
                student = request.GET.get('student_id')
                student_obj = Student.objects.get(id=student)

            
                serializer = StudentSerializer(student_obj)
            return Response({'status':'success','data':serializer.data},status=200)
        except Exception as e:
            return Response({'status':'ERROR', 'message':str(e)},status=400)            
    def post(self, request, *args, **kwargs):
        
        try:
            if request.user.user_type == 'T':
                print(request.user.user_type)
                serializer = StudentSerializer(data=request.data)
                serializer.is_valid(raise_exception=True)
                serializer.save()
            
           
            return Response({'status':'success','data':serializer.data},status=200)

        except Exception as e:
            return Response({'status':'ERROR', 'message':str(e)},status=400)        