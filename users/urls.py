from django.urls import path,include
from .import views as user_api
from rest_framework_simplejwt import views as jwt_views

app_name = 'users'


"""
    In this url we enter the email whose password we want to recover 
    password reset url :- http://127.0.0.1:8000/accounts/api/password_reset/

    This is the confirmation url where we enter the token recieved in the email and the new password
    password reset confirm :- http://127.0.0.1:8000/accounts/api/password_reset/confirm/
"""

    
urlpatterns = [
   
    path('api/register/', user_api.CreateUserView.as_view(), name='register'),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),
    path('api/student/', user_api.StudentView.as_view(), name='create_student'),
    
]    

