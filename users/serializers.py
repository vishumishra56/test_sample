from rest_framework import serializers
from django.contrib.auth import get_user_model 
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType 
from .models import Student,Teacher
UserModel = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)

    def create(self, validated_data):

        user = UserModel.objects.create(
            email=validated_data['email'],first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],mobile=validated_data['mobile'],user_type=validated_data['user_type']
        )
        user.set_password(validated_data['password'])
        if user.user_type == 'S':
            ct = ContentType.objects.get_for_model(Student)
            permission = Permission.objects.create(codename ='can_view_student', 
                                        name ='Can View Student', 
                                                content_type = ct)
            student_group, created = Group.objects.get_or_create(name='Student')
            student_group.permissions.add(permission) 
            user.groups.add(student_group)
        elif user.user_type == 'T':
            ct = ContentType.objects.get_for_model(Student,Teacher)
            permission = Permission.objects.create(codename =['can_view_student','can_add_student','can_change_student'
            'can_delete_student'], 
                                        name ='Can View Student', 
                                                content_type = ct)
            teacher_group, created = Group.objects.get_or_create(name='Teacher')
            teacher_group.permissions.add(permission) 
            user.groups.add(teacher_group)  
        else:
            # ct = ContentType.objects.get_for_model(Student,Teacher)
            # permission = Permission.objects.create(codename =['can_view_student','can_add_student','can_change_student'
            # 'can_delete_student'], 
            #                             name ='Can View Student', 
            #                                     content_type = ct)
            per = Permission.objects.all() 
            for i in per:
                print(i)                                   
            admin_group, created = Group.objects.get_or_create(name='Admin')
            admin_group.permissions.add(*per) 
            user.groups.add(admin_group)  


        user.save()

        return user

    class Meta:
        model = UserModel
        # Tuple of serialized model fields (see link [2])
        fields = ( "id", "email", "password","first_name","last_name","mobile","user_type")


        
class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        # Tuple of serialized model fields (see link [2])
        fields = '__all__'